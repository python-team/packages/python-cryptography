Source: python-cryptography
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jérémy Lal <kapouer@melix.org>,
           Andrey Rakhmatullin <wrar@debian.org>,
Section: python
Priority: optional
Build-Depends: cargo,
               debhelper-compat (= 13),
               dh-sequence-python3,
               librust-asn1-0.17-dev,
               librust-cc-1.1-dev,
               librust-cfg-if-dev,
               librust-foreign-types-0.3-dev,
               librust-foreign-types-shared-0.1-dev,
               librust-once-cell-dev,
               librust-openssl-dev (>= 0.10.64~),
               librust-openssl-sys-dev (>= 0.9.101~),
               librust-pem-3.0-dev,
               librust-pyo3-0.22+default-dev,
               librust-pyo3-0.22-dev,
               librust-self-cell-dev,
               libssl-dev,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-bcrypt <!nocheck>,
               python3-certifi (>= 2024) <!nocheck>,
               python3-cffi,
               python3-cryptography-vectors (<< 43.0.1~) <!nocheck>,
               python3-cryptography-vectors (>= 43.0.0~) <!nocheck>,
               python3-maturin,
               python3-pretend <!nocheck>,
               python3-pytest (>= 7.4.0) <!nocheck>,
               python3-pytest-benchmark (>= 4.0) <!nocheck>,
Build-Depends-Indep: dh-sequence-sphinxdoc <!nodoc>,
                     python3-doc <!nodoc>,
                     python3-sphinx <!nodoc>,
                     python3-sphinx-rtd-theme <!nodoc>,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-cryptography
Vcs-Git: https://salsa.debian.org/python-team/packages/python-cryptography.git
Homepage: https://cryptography.io/
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: python3-cryptography
Architecture: any
Depends: python3-bcrypt,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Suggests: python-cryptography-doc,
          python3-cryptography-vectors,
Description: Python library exposing cryptographic recipes and primitives (Python 3)
 The cryptography library is designed to be a "one-stop-shop" for
 all your cryptographic needs in Python.
 .
 As an alternative to the libraries that came before it, cryptography
 tries to address some of the issues with those libraries:
  - Lack of PyPy and Python 3 support.
  - Lack of maintenance.
  - Use of poor implementations of algorithms (i.e. ones with known
    side-channel attacks).
  - Lack of high level, "Cryptography for humans", APIs.
  - Absence of algorithms such as AES-GCM.
  - Poor introspectability, and thus poor testability.
  - Extremely error prone APIs, and bad defaults.
 .
 This package contains the Python 3 version of cryptography.

Package: python-cryptography-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
Built-Using: ${sphinxdoc:Built-Using},
Description: Python library exposing cryptographic recipes and primitives (documentation)
 The cryptography library is designed to be a "one-stop-shop" for
 all your cryptographic needs in Python.
 .
 As an alternative to the libraries that came before it, cryptography
 tries to address some of the issues with those libraries:
  - Lack of PyPy and Python 3 support.
  - Lack of maintenance.
  - Use of poor implementations of algorithms (i.e. ones with known
    side-channel attacks).
  - Lack of high level, "Cryptography for humans", APIs.
  - Absence of algorithms such as AES-GCM.
  - Poor introspectability, and thus poor testability.
  - Extremely error prone APIs, and bad defaults.
 .
 This package contains the documentation for cryptography.
Build-Profiles: <!nodoc>
Multi-Arch: foreign
