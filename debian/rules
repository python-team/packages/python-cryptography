#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk
include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/buildflags.mk
include /usr/share/rustc/architecture.mk
export CFLAGS CXXFLAGS CPPFLAGS LDFLAGS
export DEB_HOST_RUST_TYPE DEB_HOST_GNU_TYPE

export CARGO=/usr/share/cargo/bin/cargo
export CARGO_HOME=$(shell pwd)/debian/cargo_home
export DEB_CARGO_CRATE=$(DEB_SOURCE)_$(DEB_VERSION_UPSTREAM)

export DEB_BUILD_MAINT_OPTIONS=hardening=+bindnow

export PYBUILD_NAME=cryptography
export PYBUILD_TEST_PYTEST=1
# needs newer librust-openssl-dev (#1081226)
export PYBUILD_TEST_ARGS={dir}/tests/ -k 'not (TestDHPublicKeySerialization and test_public_bytes_match)'

%:
	dh $@ --buildsystem=pybuild


override_dh_auto_clean:
	if [ -f Cargo.lock.orig ] ; then mv Cargo.lock.orig Cargo.lock ; fi
	dh_auto_clean
	rm -rf src/rust/target
	rm -rf debian/cargo_registry


override_dh_sphinxdoc:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=./src python3 -m sphinx -b html -N docs/ \
		   $(CURDIR)/debian/python-cryptography-doc/usr/share/doc/python-cryptography-doc/html
	dh_sphinxdoc
endif


override_dh_auto_configure:
	$(CARGO) prepare-debian debian/cargo_registry --link-from-system
	mkdir -p src/rust/target/release
	ln -sf ../$(DEB_HOST_RUST_TYPE)/release/libcryptography_rust.so src/rust/target/release/libcryptography_rust.so
	mv Cargo.lock Cargo.lock.orig
	# needed because we patch Cargo.toml
	cargo generate-lockfile --offline
	dh_auto_configure


override_dh_dwz:
	dh_dwz --no-dwz-multifile


override_dh_python3:
	# We patch it out of .dist-info to avoid pkg_resources complaining (#1026537)
	dh_python3 --depends=cffi
